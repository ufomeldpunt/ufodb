<?php

use App\Http\Controllers\Identifier\IndexController as IdentifierIndexController;
use App\Http\Controllers\Identifier\UpdateController as IdentifierUpdateController;
use App\Http\Controllers\Report\CreateController as ReportCreateController;
use App\Http\Controllers\Report\ShowController as ReportShowController;

use App\Http\Controllers\UtilController;

use CodeZero\LocalizedRoutes\Controller\FallbackController;
use CodeZero\LocalizedRoutes\Middleware\SetLocale;
use Illuminate\Support\Facades\Route;

Route::localized(function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('home');

    /**
     * Identifier
     */
    Route::name('identifier.')->group(function () {
        Route::get(Lang::uri('report-ufo'), IdentifierIndexController::class)->name('index');
    });

    /**
     * Report
     */
    Route::name('report.')->group(function () {
        Route::get(Lang::uri('report'), ReportShowController::class)->whereNumber('id')->name('show');
    });
});

/**
 * Identifier
 */
Route::name('identifier.')->group(function () {
    Route::post('identifier/update', IdentifierUpdateController::class)->name('update');
});

/**
 * Util
 */
Route::name('util.')->group(function () {
    Route::get('util/languages/{country_iso}', [UtilController::class, 'languages'])->where('country_iso', '[a-z]{2}')->name('languages');
});

Route::fallback(FallbackController::class)->middleware(SetLocale::class);