<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CountryLanguageSeeder extends Seeder
{
    public function run()
    {
        $rows = json_decode(File::get(base_path('database/seeders/json/country_language.json')));

        if ($rows) {
            foreach ($rows as $row) {
                DB::table('country_language')->insert([
                    'country_iso' => $row->country_iso,
                    'language_iso' => $row->language_iso,
                ]);
            }
        }
    }
}
