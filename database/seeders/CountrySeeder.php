<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CountrySeeder extends Seeder
{
    public function run()
    {
        $countries = json_decode(File::get(base_path('database/seeders/json/countries.json')));

        if ($countries) {
            foreach ($countries as $country) {
                DB::table('countries')->insert([
                    'iso' => $country->iso,
                ]);
            }
        }
    }
}
