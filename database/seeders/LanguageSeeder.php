<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class LanguageSeeder extends Seeder
{
    public function run()
    {
        $languages = json_decode(File::get(base_path('database/seeders/json/languages.json')));

        if ($languages) {
            foreach ($languages as $language) {
                DB::table('languages')->insert([
                    'iso' => $language->iso,
                    'name' => $language->name,
                ]);
            }
        }
    }
}
