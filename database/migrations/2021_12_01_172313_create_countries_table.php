<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->char('iso', 2)->unique();
            $table->primary('iso');
        });
    }

    public function down()
    {
        Schema::dropIfExists('country_language');
        Schema::dropIfExists('countries');
    }
}
