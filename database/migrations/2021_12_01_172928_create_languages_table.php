<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguagesTable extends Migration
{
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            $table->char('iso', 3)->unique();
            $table->string('name');
            $table->primary('iso');
        });
    }

    public function down()
    {
        Schema::dropIfExists('country_language');
        Schema::dropIfExists('languages');
    }
}
