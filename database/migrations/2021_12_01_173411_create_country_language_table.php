<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryLanguageTable extends Migration
{
    public function up()
    {
        Schema::create('country_language', function (Blueprint $table) {
            $table->char('country_iso', 2)->index();
            $table->char('language_iso', 3)->index();
            $table->foreign('country_iso')->references('iso')->on('countries')->onDelete('cascade');
            $table->foreign('language_iso')->references('iso')->on('languages')->onDelete('cascade');
            $table->primary(['country_iso', 'language_iso']);
        });
    }

    public function down()
    {
        // Schema::table('country_language', function (Blueprint $table) {
        //     $table->dropForeign('country_language_country_iso_foreign');
        //     $table->dropForeign('country_language_language_iso_foreign');
        // });

        Schema::dropIfExists('country_languages');
    }
}
