<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShowController extends Controller
{
    public function __invoke(Request $request, string $slug, int $id)
    {
        return view('report/show', [
            'id' => $id,
        ]);
    }
}
