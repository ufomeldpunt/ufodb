<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Language;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UtilController extends Controller
{
    public function languages(Request $request, string $country_iso)
    {
        $country = Country::with('languages')->find($country_iso);
        $current = $request->input('current');
        $current_language = Language::find($current);
        $user = $request->input('user');
        $user_languages = Language::whereIn('iso', explode(',', $user))->get();
    
        $response = [
            'country' => $country->languages ?? [],
            'current' => $current_language,
            'user' => $user_languages ?? [],
        ];
        
        return response()->json($response);
    }
}
