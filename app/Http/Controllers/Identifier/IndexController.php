<?php

namespace App\Http\Controllers\Identifier;

use App\Models\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke(Request $request)
    {
        $countries = Country::with('languages')->get();

        return view('identifier/index', [
            'countries' => $countries,
        ]);
    }
}
