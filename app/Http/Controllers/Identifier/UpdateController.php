<?php

namespace App\Http\Controllers\Identifier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    public function __invoke(Request $request)
    {
        $q = 0;
        $response = [];

        if ($request->input('report')) {
            $request->session()->put('identifier_report', 1);
            $response['location'] = route('report.create');
        } else {
            $answers = [];

            foreach ($request->all() as $key => $value) {
                if (preg_match('/^q(\d+)$/', $key, $matches)) {
                    $answers[] = $key . $value;
                    $q = intval($matches[1]);
                }
            }

            sort($answers);
            $answers = implode(',', $answers);
            $request->session()->put('identifier_answers', $answers);

            $suggestions = [
                'q1b' => [ // afterwards
                    'backscatter',
                    'bird_insect',
                    'dirt',
                    'droplet',
                    'lens_flare',
                    'reflection',
                ],
                'q1a,q2a,q3a' => [ // live, moving consistently, illuminated
                    'aircraft',
                    'contrail',
                    'inspection_train',
                    'meteor',
                    'satellite',
                    'starlink',
                    'thai_lantern',
                ],
                'q1a,q2a,q3b' => [ // live, moving consistently, not illuminated
                    'contrail',
                    'foil_balloon',
                    'hot_air_balloon',
                    'plastic_bag',
                    'solar_balloon',
                    'unusual_aircraft',
                    'weather_balloon',
                ],
                'q1a,q2b,q3a' => [ // live, moving irregularly, illuminated
                    'drone',
                    'helicopter',
                    'promotional_lights',
                ],
                'q1a,q2b,q3b' => [ // live, moving irregularly, not illuminated
                    'foil_balloon',
                    'plastic_bag',
                ],
                'q1a,q2c,q3a' => [ // live, stationary, illuminated
                    'art_project',
                    'beacon_lights',
                    'helicopter',
                    'industrial_lights',
                    'inspection_train',
                    'lightning',
                    'planet',
                    'promotional_lights',
                    'star',
                ],
                'q1a,q2c,q3b' => [ // live, stationary, not illuminated
                    'helicopter',
                    'kite',
                    'praying_bird',
                    'smoke_ring',
                    'sundog',
                    'unusual_cloud',
                    'zeppelin',
                ],
            ];

            if (array_key_exists($answers, $suggestions)) {
                $identifications = Identification::whereIn('key', $suggestions[$answers])->get();
                $response['result_html'] = view('identifier.result', ['identifications' => $identifications])->render();
            }
        }

        if (array_key_exists('result_html', $response)) {
            $response['step'] = $q;
        } else {
            $response['step'] = $q + 1;
        }

        return response()->json($response);
    }
}
