<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
    protected $primaryKey = 'iso';

    function languages()
    {
        return $this->belongsToMany(Language::class, 'country_language', 'country_iso', 'language_iso', 'iso', 'iso');
    }
}
