<?php

namespace App\Models;

use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public $incrementing = false;

    protected $keyType = 'string';
    protected $primaryKey = 'iso';

    function countries()
    {
        return $this->belongsToMany(Country::class, 'country_language', 'language_iso', 'country_iso', 'iso', 'iso');
    }
}
