<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $country_codes = [
        'ax', // Åland
        'al', // Albania
        'ad', // Andorra
        'at', // Austria
        'by', // Belarus
        'be', // Belgium
        'ba', // Bosnia and Herzegovina
        'bg', // Bulgaria
        'hr', // Croatia
        'cy', // Cyprus
        'cz', // Czech Republic
        'dk', // Denmark
        'ee', // Estonia
        'fo', // Faroe Islands
        'fi', // Finland
        'fr', // France
        'de', // Germany
        'gi', // Gibraltar
        'gr', // Greece
        'gg', // Guernsey
        'hu', // Hungary
        'is', // Iceland
        'ie', // Ireland
        'im', // Isle of Man
        'it', // Italy
        'je', // Jersey
        'xk', // Kosovo
        'lv', // Latvia
        'li', // Liechtenstein
        'lu', // Luxembourg
        'mk', // Macedonia
        'mt', // Malta
        'md', // Moldova
        'mc', // Monaco
        'me', // Montenegro
        'nl', // Netherlands
        'no', // Norway
        'pl', // Poland
        'pt', // Portugal
        'rs', // Republic of Serbia
        'ro', // Romania
        'ru', // Russia
        'sm', // San Marino
        'sk', // Slovakia
        'si', // Slovenia
        'es', // Spain
        'se', // Sweden
        'ch', // Switzerland
        'ua', // Ukraine
        'gb', // United Kingdom
        'va', // Vatican
    ];

    sort($country_codes);
    $country_names = [];

    foreach ($country_codes as $code) {
        $url = sprintf('https://api.bigdatacloud.net/data/country-info?code=%s&localityLanguage=nl&key=65a911104e494985b0389a17cc472432', $code);

        if ($response = @file_get_contents($url)) {
            $response = json_decode($response);
            $country_names['countries.' . $code] = $response->name;
        }
    }

    header('Content-Type: application/json; charset=utf-8');
    die(json_encode($country_names, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT));
?>