<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $countries = json_decode(file_get_contents('countries.json'));
    $languages = [];
    $languages_rows = [];

    foreach ($countries as $country) {
        $isos = explode(',', $country->language_iso);
        $names = explode(',', $country->language);

        for ($i = 0; $i < count($isos); $i++) {
            $iso = $isos[$i];
            $name = $names[$i];
            $languages[$iso] = strtolower($name);
        }
    }

    ksort($languages);

    foreach ($languages as $iso => $name) {
        $languages_rows[] = [
            'iso' => $iso,
            'name' => $name,
        ];
    }

    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($languages_rows, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
?>