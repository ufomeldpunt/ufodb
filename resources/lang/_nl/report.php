<?php

return [
    'create' => [
        'h1' => 'UFO melden',
    ],

    'show' => [
        'date' => 'datum',
        'time' => 'tijd',
    ],
];