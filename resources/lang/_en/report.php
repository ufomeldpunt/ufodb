<?php

return [
    'create' => [
        'h1' => 'Report a UFO',
    ],

    'show' => [
        'date' => 'date',
        'time' => 'time',
    ],
];