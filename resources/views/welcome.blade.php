@extends('layouts.app', ['has_map' => true])

@section('title', __('home.h1'))

@section('content')
    <h1>@yield('title')</h1>
    <div id="map" style="height: 75vh; width: 100%;"></div>
    <script>
        const map = new maplibregl.Map({
            container: 'map',
            style: 'https://basemaps.cartocdn.com/gl/voyager-gl-style/style.json',
            // style: 'https://basemaps.cartocdn.com/gl/positron-gl-style/style.json',
            center: [0, 0],
            zoom: 1
        });

        map.addControl(new maplibregl.NavigationControl());
    </script>
@endsection