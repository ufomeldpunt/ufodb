<div aria-role="modal" class="c-languageSelector js-languageSelector">
    <div class="c-languageSelector__window">
        <h2 class="c-languageSelector__title">{{ __('language_selector.title') }}</h2>
        <div class="js-languageSelectorLanguages"></div>
    </div>
    <script id="languageSelectorTemplate" type="text/template">
        <ul class="c-languageSelector__list">
            <% _.forEach(languages, language => { %>
                <li class="c-languageSelector__listItem">
                    <a class="c-languageSelector__link js-languageSelectorLink <%- language.isCurrent ? 'is-current' : '' %>" data-iso="<%- language.iso %>" href="<%- language.href %>" tabindex="0">
                        <img alt="<%- _.upperFirst(language.name) %>" class="c-languageSelector__flag" src="/assets/img/flags/languages/<%- language.iso %>.svg">
                        <span class="c-languageSelector__language"><%- _.upperFirst(language.name) %></span>
                    </a>
                </li>
            <% }) %>
        </ul>
    </script>
</div>