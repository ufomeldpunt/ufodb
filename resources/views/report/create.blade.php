@extends('layouts.app')

@section('title', __('report.create.h1'))

@section('content')
    <h1>@yield('title')</h1>
    <p>
        {{ route('report.create', [], true, 'nl') }}
    </p>
@endsection
