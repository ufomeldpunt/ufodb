@extends('layouts.app')

@section('title', __('about.h1'))

@section('content')
    {{ app()->getLocale() }}<br>
    {{ route('report.show', ['slug' => 'alex', 'id' => 123]) }}<br>
    {{ route('report.create') }}<br>
@endsection