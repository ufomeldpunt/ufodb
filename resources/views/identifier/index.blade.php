@extends('layouts.app')

@section('title', __('identifier.h1'))

@section('content')
    <div class="o-page">
        <div class="o-wrapper o-wrapper--narrow">
            @foreach ($countries as $country)
                <h2>{{ $country->iso }}</h2>
                <ul>
                    @foreach ($country->languages as $language)
                        <li>{{ $language->iso }}</li>
                    @endforeach
                </ul>
            @endforeach
            <form action="{{ route('identifier.update') }}" class="c-identifier js-identifier" data-identifier-current-step="1" method="post">
                @csrf
                <div class="c-section">
                    <h1 class="u-m-m">{{ __('identifier.h1') }}</h1>
                    <p class="u-m-xxl">
                        {{ __('identifier.intro') }}
                    </p>
                    <ul class="c-identifier__steps o-wrapper__bleed">
                        <li class="c-identifier__step js-identifierStep o-wrapper__bleed" data-identifier-step="1">
                            <p class="c-identifier__question">{{ __('identifier.q1_label') }}</p>
                            <div class="c-identifier__answers">
                                <div class="c-identifier__answer">
                                    <input class="c-identifier__answerInput js-identifierAnswerInput" data-identifier-step="1" id="q1a" name="q1" tabindex="0" type="radio" value="a">
                                    <label role="button" class="c-identifier__answerLabel" for="q1a" tabindex="-1"><span>{{ __('identifier.q1.a') }}</span></label>
                                </div>
                                <div class="c-identifier__answer">
                                    <input class="c-identifier__answerInput js-identifierAnswerInput" data-identifier-step="1" id="q1b" name="q1" tabindex="0" type="radio" value="b">
                                    <label role="button" class="c-identifier__answerLabel" for="q1b" tabindex="-1"><span>{{ __('identifier.q1.b') }}</span></label>
                                </div>
                            </div>
                        </li>
                        <li class="c-identifier__step js-identifierStep o-wrapper__bleed" data-identifier-step="2">
                            <p class="c-identifier__question">{{ __('identifier.q2_label') }}</p>
                            <div class="c-identifier__answers">
                                <div class="c-identifier__answer">
                                    <input class="c-identifier__answerInput js-identifierAnswerInput" data-identifier-step="2" id="q2a" name="q2" type="radio" value="a">
                                    <label class="c-identifier__answerLabel" for="q2a"><span>{{ __('identifier.q2.a') }}</span></label>
                                </div>
                                <div class="c-identifier__answer">
                                    <input class="c-identifier__answerInput js-identifierAnswerInput" data-identifier-step="2" id="q2b" name="q2" type="radio" value="b">
                                    <label class="c-identifier__answerLabel" for="q2b"><span>{{ __('identifier.q2.b') }}</span></label>
                                </div>
                                <div class="c-identifier__answer">
                                    <input class="c-identifier__answerInput js-identifierAnswerInput" data-identifier-step="2" id="q2c" name="q2" type="radio" value="c">
                                    <label class="c-identifier__answerLabel" for="q2c"><span>{{ __('identifier.q2.c') }}</span></label>
                                </div>
                            </div>
                        </li>
                        <li class="c-identifier__step js-identifierStep o-wrapper__bleed" data-identifier-step="3">
                            <p class="c-identifier__question">{{ __('identifier.q3_label') }}</p>
                            <div class="c-identifier__answers">
                                <div class="c-identifier__answer">
                                    <input class="c-identifier__answerInput js-identifierAnswerInput" data-identifier-step="3" id="q3a" name="q3" type="radio" value="a">
                                    <label class="c-identifier__answerLabel" for="q3a"><span>{{ __('identifier.q3.a') }}</span></label>
                                </div>
                                <div class="c-identifier__answer">
                                    <input class="c-identifier__answerInput js-identifierAnswerInput" data-identifier-step="3" id="q3b" name="q3" type="radio" value="b">
                                    <label class="c-identifier__answerLabel" for="q3b"><span>{{ __('identifier.q3.b') }}</span></label>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="c-identifier__result js-identifierResult"></div>
            </form>
        </div>
    </div>
@endsection