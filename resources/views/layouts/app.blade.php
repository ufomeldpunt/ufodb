<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="initial-scale=1,maximum-scale=1,width=device-width">
        <title>@yield('title') - UFODb</title>
        @foreach (Config::get('localized-routes.supported-locales') as $locale)
            @if ($locale !== app()->getLocale())
                <link href="{{ \Route::localizedUrl($locale) }}" hreflang="{{ $locale }}" rel="alternate">
            @endif
        @endforeach
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+Mono&family=Noto+Sans:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">
        <link href="/assets/css/app.css" rel="stylesheet" type="text/css">
        @if (!empty($has_map))
            <script src="https://unpkg.com/maplibre-gl@1.15.2/dist/maplibre-gl.js"></script>
            <link href="https://unpkg.com/maplibre-gl@1.15.2/dist/maplibre-gl.css" rel="stylesheet">
        @endif
    </head>
    <body>
        <nav>
            <a href="{{ route('home') }}">UFODb</a>
            <a href="{{ route('identifier.index') }}">{{ __('ui.report_ufo') }}</a>
        </nav>
        @yield('content')
        @include('partials/language_selector')
        <script src="/assets/js/app.js"></script>
        @yield('scripts')
    </body>
</html>