import axios from 'axios'

export default class Upload
{
    constructor($el, options = {}) {
        this.DOM = { $el }
        this.options = { ...Upload.prototype.defaults, ...options }
        this.init()
    }

    init() {
        // Set DOM elements
        this.DOM.$preview = this.DOM.$el.querySelector(this.options.selectorPreview)
        this.DOM.$progressBarFill = this.DOM.$el.querySelector(this.options.selectorProgressBarFill)
        this.DOM.$remove = this.DOM.$el.querySelector(this.options.selectorRemove)

        // Set options
        this.options.url = this.DOM.$el.dataset.uploadUrl || this.options.url

        // Set properties
        this.cancelTokenSource = axios.CancelToken.source()
        this.deleteToken = this.DOM.$el.dataset.uploadDeleteToken || null
        this.file = this.DOM.$el.file
        this.isUploaded = (this.DOM.$el.dataset.uploadDeleteToken !== undefined)

        // Register listeners
        this.DOM.$preview.addEventListener('click', this.onPreviewClick.bind(this), false)
        this.DOM.$remove.addEventListener('click', this.onRemoveClick.bind(this), false)

        // Run!
        if (this.file) {
            this.setPreview()
            this.upload()
        }
    }

    setPreview() {
        const reader = new FileReader()

        reader.onload = (e) => {
            this.DOM.$preview.src = e.target.result
        }

        reader.readAsDataURL(this.file)
    }

    onPreviewClick() {
        const $zoomPreview = document.createElement('div')
        $zoomPreview.classList.add(this.options.classNameZoomPreview)

        const $zoomPreviewImage = document.createElement('img')
        $zoomPreviewImage.classList.add(this.options.classNameZoomPreviewImage)
        $zoomPreviewImage.src = this.DOM.$preview.src

        $zoomPreview.addEventListener('click', (e) => e.currentTarget.remove(), false)
        $zoomPreview.appendChild($zoomPreviewImage)

        document.body.appendChild($zoomPreview)
    }

    onRemoveClick() {
        if (this.isUploaded) {
            // Remove upload
            if (this.deleteToken) {
                axios.delete(this.options.url + '/' + this.deleteToken)
            }
        } else {
            // Cancel upload
            this.cancelTokenSource.cancel()
        }

        this.DOM.$el.dispatchEvent(new CustomEvent('upload:remove', {bubbles: true}))
        this.DOM.$el.remove()
    }

    upload() {
        const config = {
            cancelToken: this.cancelTokenSource.token,
            onUploadProgress: (e) => {
                this.DOM.$progressBarFill.style.width = ((e.loaded * 100) / e.total) + '%'
            }
        }

        let data = new FormData()
        data.append('file', this.file)
        data.append('MAX_FILE_SIZE', 10000000)
        data.append('PHP_SESSION_UPLOAD_PROGRESS', 'progress')

        axios.post(this.options.url, data, config)
            .then(response => {
                this.DOM.$el.classList.add(this.options.classNameIsUploaded)
                this.deleteToken = response.data.delete_token || null
                this.isUploaded = true
            })
            .catch(exception => {
                if (axios.isCancel(exception)) {
                    // Upload cancelled
                } else {
                    // Handle error
                }
            })
    }
}

Upload.prototype.defaults = {
    classNameIsUploaded: 'is-uploaded',
    classNameZoomPreview: 'c-upload__zoomPreview',
    classNameZoomPreviewImage: 'c-upload__zoomPreviewImage',
    selectorPreview: '.js-uploadPreview',
    selectorProgressBarFill: '.js-uploadProgressBarFill',
    selectorRemove: '.js-uploadRemove',
    url: null,
}