export default class InputTextarea
{
    constructor($el, options = {}) {
        this.DOM = { $el }
        this.options = { ...InputTextarea.prototype.defaults, ...options }
        this.init()
    }

    init() {
    }
}

InputTextarea.prototype.defaults = {
}