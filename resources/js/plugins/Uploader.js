export default class Uploader
{
    constructor($el, options = {}) {
        this.DOM = { $el }
        this.options = { ...Uploader.prototype.defaults, ...options }
        this.init()
    }

    formatSize(size) {
        if (size > 1000000) {
            return (Math.round((size / 1000 / 1000) * 10) / 10) + ' MB'
        }

        return Math.round(size / 1000) + ' KB' 
    }

    init() {
        // Set DOM elements
        this.DOM.$input = this.DOM.$el.querySelector(this.options.selectorInput)
        this.DOM.$upload = this.DOM.$el.querySelector(this.options.selectorUpload)
        this.DOM.$uploads = this.DOM.$el.querySelector(this.options.selectorUploads)

        // Set options
        this.options.types = this.DOM.$el.dataset.uploaderTypes || this.options.types
        this.options.limit = this.DOM.$el.dataset.uploaderLimit || this.options.limit
        this.options.maxFileSize = this.DOM.$el.dataset.uploaderMaxFileSize || this.options.maxFileSize

        // Set properties
        this.numUploads = this.DOM.$uploads.children.length

        if (this.DOM.$upload) {
            this.upload = _.template(this.DOM.$upload.innerHTML)
        }

        // Register listeners
        this.DOM.$input.addEventListener('change', this.onInputChange.bind(this), false)
        this.DOM.$el.addEventListener('upload:remove', this.onUploadRemove.bind(this), false)

        // Run!
        this.update()
    }

    onInputChange(e) {
        const files = this.DOM.$input.files
        let $upload
        let $ol
        let file

        for (let i = 0; i < files.length; i++) {
            file = files[0]

            if (i < (this.options.limit - this.numUploads)) {
                if (this.validate(file)) {
                    $ol = document.createElement('ol')
                    $ol.innerHTML = this.upload({
                        name: file.name,
                        size: this.formatSize(file.size),
                    })

                    $upload = $ol.firstElementChild
                    $upload.file = file
 
                    this.DOM.$uploads.appendChild($upload)
                    this.numUploads += 1
                }
            }
        }

        this.DOM.$input.value = null
        this.update()
    }

    onUploadRemove() {
        this.numUploads -= 1
        this.update()
    }

    update() {
        if (this.numUploads === 0) {
            this.DOM.$uploads.innerHTML = ''
        }

        this.DOM.$el.classList.toggle(this.options.classNameIsMaxUploads, (this.numUploads >= this.options.limit))
    }

    validate(file) {
        const types = this.options.types.split(',')

        if (types.indexOf(file.type) === -1) {
            alert('Invalid file type.')
            return false
        }

        if (file.size > this.options.maxFileSize) {
            alert('File too big.')
            return false
        }

        return true
    }
}

Uploader.prototype.defaults = {
    classNameIsMaxUploads: 'is-maxUploads',
    limit: 1,
    maxFileSize: 5000000, // 5MB
    types: 'image/jpeg,image/png',
    selectorInput: '.js-uploaderInput',
    selectorUpload: '.js-uploaderUpload',
    selectorUploads: '.js-uploaderUploads',
}