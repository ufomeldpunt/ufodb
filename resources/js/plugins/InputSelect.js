export default class InputSelect
{
    constructor($el, options = {}) {
        this.DOM = { $el }
        this.options = { ...InputSelect.prototype.defaults, ...options }
        this.init()
    }

    init() {
        this.DOM.$el.classList.add(this.options.classNameSelect)

        // Add wrapper
        this.DOM.$wrapper = document.createElement('div')
        this.DOM.$wrapper.classList.add(this.options.classNameWrapper)
        this.DOM.$el.parentNode.insertBefore(this.DOM.$wrapper, this.DOM.$el)
        this.DOM.$wrapper.appendChild(this.DOM.$el)

        // Add label
        this.DOM.$label = document.createElement('div')
        this.DOM.$label.classList.add(this.options.classNameLabel)
        this.DOM.$label.innerText = this.getLabel()
        this.DOM.$el.parentNode.insertBefore(this.DOM.$label, this.DOM.$el.nextSibling)

        // Add event listeners
        this.DOM.$el.addEventListener('change', this.onChange.bind(this))
    }

    getLabel() {
        if (this.DOM.$el.selectedIndex > -1) {
            return this.DOM.$el.options[this.DOM.$el.selectedIndex].innerText.trim()
        }

        return this.DOM.$el.options[0].innerText.trim()
    }

    onChange() {
        this.DOM.$label.innerText = this.getLabel()
    }
}

InputSelect.prototype.defaults = {
    classNameFocus: 'is-focus',
    classNameLabel: 'c-inputSelect__label',
    classNameSelect: 'c-inputSelect__select',
    classNameWrapper: 'c-inputSelect',
}