export default class XhrForm
{
    constructor($el, options = {}) {
        this.DOM = { $el }
        this.options = { ...XhrForm.prototype.defaults, ...options }
        this.init()
    }

    init() {
        this.DOM.$errors = this.DOM.$el.querySelector(this.options.selectorErrors)

        this.headers = {
            'Accept': 'application/json',
            'Cache-Control': 'no-cache',
            'X-CSRF-TOKEN': document.querySelector('meta[name="_token"]').getAttribute('content'),
            'X-Requested-With': 'XMLHttpRequest',
            ...this.options.headers,
        }
        this.method = this.DOM.$el.getAttribute('method')
        this.url = this.DOM.$el.getAttribute('action')

        this.DOM.$el.addEventListener('submit', this.onSubmit.bind(this))
    }

    displayErrors(errors) {
        let $error
        let $field
        let $fieldError
        let $fieldErrors
        let $input
        let name

        for (const key in errors) {
            name = key.replace(/\./g, '[') + Array(key.split('.').length).join(']')
            $input = this.DOM.$el.querySelector(`[name="${name}"]`)

            if (!$input) {
                $input = this.DOM.$el.querySelector(`[name="${name}[]"]`) 
            }

            if ($input) {
                $field = $input.closest(this.options.selectorField)

                if ($field) {
                    $field.classList.add(this.options.classNameHasError)
                    $fieldErrors = $field.querySelector(this.options.selectorFieldErrors)

                    if ($fieldErrors) {
                        errors[key].forEach(error => {
                            $fieldError = document.createElement('li')
                            $fieldError.innerHTML = error
                            $fieldErrors.appendChild($fieldError)
                        })
                    }
                }
            }

            if (this.DOM.$errors) {
                errors[key].forEach(error => {
                    $error = document.createElement('li')
                    $error.innerHTML = error
                    this.DOM.$errors.appendChild($error)
                })
            }
        }
    }

    displayError(error) {
        let $error
        if (this.DOM.$errors) {
            $error = document.createElement('li');
            $error.innerHTML = error;
            this.DOM.$errors.appendChild($error);
            this.DOM.$errors.classList.remove('u-hide');
        }
    }

    onSubmit(e) {
        e.preventDefault()

        const { headers, method, url } = this
        const data = new FormData(this.DOM.$el)

        if (this.DOM.$el.dataset[this.options.dataAttrConfirm]) {
            if (!window.confirm(this.DOM.$el.dataset[this.options.dataAttrConfirm])) {
                return false
            }
        }

        this.DOM.$el.classList.add(this.options.classNameIsBusy)
        this.DOM.$el.dispatchEvent(new CustomEvent('xhrform:beforesend'))

        axios({ data, headers, method, url })
            .then(response => {
                this.reset()
                this.DOM.$el.classList.add(this.options.classNameIsSuccess)
                this.DOM.$el.classList.remove(this.options.classNameIsBusy)
                this.DOM.$el.dispatchEvent(new CustomEvent('xhrform:complete', { detail: response.data }))
                this.DOM.$el.dispatchEvent(new CustomEvent('xhrform:success', { detail: response.data }))
            })
            .catch(error => {
                this.reset()
                this.DOM.$el.classList.remove(this.options.classNameIsBusy)
                this.DOM.$el.dispatchEvent(new CustomEvent('xhrform:complete', { detail: error.response.data }))

                switch (error.response.status) {
                    case 302:
                        if (error.response.data.hasOwnProperty('location')) {
                            this.DOM.$el.dispatchEvent(new CustomEvent('xhrform:beforeredirect', { detail: error.response.data }))
                            window.location = error.response.data.location
                        }

                        break;

                    case 422:
                        this.DOM.$el.classList.add(this.options.classNameHasErrors)
                        this.DOM.$el.dispatchEvent(new CustomEvent('xhrform:validationerror'))
                        if(error.response.data.result === 'error') {
                            this.displayError(error.response.data.message);
                        }else{
                            this.displayErrors(error.response.data.errors);
                        }
                        break;

                    default:
                        this.DOM.$el.classList.add(this.options.classNameHasServerError)
                        this.DOM.$el.dispatchEvent(new CustomEvent('xhrform:servererror'))
                        break;
                }
            })

        this.DOM.$el.dispatchEvent(new CustomEvent('xhrform:send', { detail: data }))
    }

    reset() {
        this.DOM.$el.classList.remove(this.options.classNameHasErrors, this.options.classNameIsSuccess)

        for (const $el of this.DOM.$el.getElementsByClassName(this.options.classNameHasError)) {
            $el.classList.remove(this.options.classNameHasError)
        }

        for (const $el of this.DOM.$el.querySelectorAll(this.options.selectorFieldErrors)) {
            $el.innerHTML = ''
        }

        if (this.DOM.$errors) {
            this.DOM.$errors.innerHTML = ''
        }
    }
}

XhrForm.prototype.defaults = {
    dataAttrConfirm: 'xhrFormConfirm',
    classNameIsBusy: 'is-busy',
    classNameHasError: 'has-error',
    classNameHasErrors: 'has-errors',
    classNameHasServerError: 'has-serverError',
    classNameIsSuccess: 'is-success',
    headers: {},
    selectorErrors: '.js-xhrFormErrors',
    selectorField: '.js-xhrFormField',
    selectorFieldErrors: '.js-xhrFormFieldErrors',
}