import './bootstrap'

/**
 * Components
 */
import LanguageSelector from './components/LanguageSelector'
import Identifier from './components/Identifier'
import LocationPicker from './components/LocationPicker'
import Map from './components/Map'

/**
 * Plugins
 */
import InputSelect from './plugins/InputSelect'
import InputTextarea from './plugins/InputTextarea'

window.app.init([
    /**
     * Components
     */

    { name: 'identifier', obj: Identifier },
    { name: 'languageSelector', obj: LanguageSelector },
    { name: 'locationPicker', obj: LocationPicker },
    { name: 'map', obj: Map },

    /**
     * Plugins
     */
    { name: 'inputSelect', obj: InputSelect },
    { name: 'inputTextarea', obj: InputTextarea },
])