import axios from 'axios'
import Cookies from 'js-cookie'
import lodash from 'lodash'

window._ = lodash
window.axios = axios
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
window.Cookies = Cookies

window.app = {
    bindings: [],

    bind($root, name, obj, options) {
        $root.querySelectorAll(`.js-${name}`).forEach($el => {
            $el.app = $el.app || {}

            if ($el.app[name] === undefined) {
                $el.app[name] = new obj($el, options)
            }
        })
    },

    bindAll($root) {
        this.bindings.forEach(binding => {
            this.bind($root, binding.name, binding.obj, binding.options || {})
        })
    },

    init(bindings) {
        this.bindings = bindings

        const mutationObserver = new MutationObserver(mutations => {
            let targets = []

            mutations.forEach(mutationRecord => {
                if (!targets.includes(mutationRecord.target)) {
                    this.bindAll(mutationRecord.target)
                    targets.push(mutationRecord.target)
                }
            })

            document.body.dispatchEvent(new CustomEvent('domchanged'))
        })

        mutationObserver.observe(document.documentElement, {
            childList: true,
            subtree: true,
        })

        this.bindAll(document)
    }
}