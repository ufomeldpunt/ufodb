import axios from "axios";
import Cookies from "js-cookie";

export default class LanguageSelector
{
    constructor($el, options = {}) {
        this.$el = $el
        this.options = {
            classNameIsVisible: 'is-visible',
            cookieName: 'language_selector',
            selectorLanguages: '.js-languageSelectorLanguages',
            selectorLink: '.js-languageSelectorLink',
            selectorTemplate: '#languageSelectorTemplate',
            ...options
        }
        this.init()
    }

    init() {
        if (Cookies.get(this.options.cookieName)) {
            return
        }

        this.$languages = this.$el.querySelector(this.options.selectorLanguages)
        this.$template = this.$el.querySelector(this.options.selectorTemplate)
        this.template = _.template(this.$template.innerHTML)

        this.availableLanguages = {}
        this.currentLanguage = document.documentElement.lang.toLocaleLowerCase()
        this.userLanguages = []

        // Fetch available languages from rel="alternate" links in head
        document.head.querySelectorAll('link[rel="alternate"]').forEach($alternate => {
            this.availableLanguages[$alternate.hreflang] = $alternate.href
        })

        // Fetch preferred languages from browser settings
        navigator.languages.forEach(language => {
            language = language.split('-')[0]

            if (!this.userLanguages.includes(language)) {
                this.userLanguages.push(language.toLocaleLowerCase())
            }
        })

        // Get country based on IP address
        axios.get('http://ip-api.com/json')
            .then(response => {
                if (response.data.hasOwnProperty('countryCode')) {
                    const country = response.data.countryCode.toLowerCase()

                    // Get language ISO codes and names
                    axios.get('/util/languages/' + country, {
                        params: {
                            current: this.currentLanguage,
                            user: this.userLanguages.join(','),
                        }
                    }).then(response => {
                        let languages = []
    
                        // Add user languages (Accept-Language header)
                        if (response.data.user && Array.isArray(response.data.user)) {
                            response.data.user.forEach(language => {
                                if (!_.some(languages, {iso: language.iso})) {
                                    languages.push(language)
                                }
                            })
                        }
    
                        // Add country languages
                        if (response.data.country && Array.isArray(response.data.country)) {
                            response.data.country.forEach(language => {
                                if (!_.some(languages, {iso: language.iso})) {
                                    languages.push(language)
                                }
                            })
                        }
    
                        // Filter out unavailable languages
                        languages = languages.filter(language => this.availableLanguages.hasOwnProperty(language.iso))
    
                        // Add current language to beginning of list
                        if (response.data.current) {
                            languages.unshift(response.data.current)
                        }
    
                        // Add hrefs
                        languages.forEach((language, i)  => {
                            languages[i].isCurrent = false
    
                            if (this.availableLanguages.hasOwnProperty(language.iso)) {
                                languages[i].href = this.availableLanguages[language.iso]
                            }
    
                            if (language.iso === this.currentLanguage) {
                                languages[i].href = window.location.href
                                languages[i].isCurrent = true
                            }
                        })
    
                        const $languageList = this.template({ languages })
                        this.$languages.innerHTML = $languageList
                        this.$el.classList.add(this.options.classNameIsVisible)
                    }).catch(error => {
                        console.log(error)
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })

        this.initListeners()
    }

    initListeners() {
        this.$languages.addEventListener('click', e => {
            const $link = e.target.closest(this.options.selectorLink)

            if ($link && $link.dataset.iso) {
                // Set cookie
                Cookies.set(this.options.cookieName, $link.dataset.iso)
            }
        })
    }
}