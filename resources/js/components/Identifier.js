import axios from 'axios'

export default class Identifier
{
    constructor($el, options = {}) {
        this.DOM = { $el }
        this.options = { ...Identifier.prototype.defaults, ...options }
        this.init()
    }

    init() {
        this.DOM.$$answerInputs = this.DOM.$el.querySelectorAll(this.options.selectorAnswerInput)
        this.DOM.$result = this.DOM.$el.querySelector(this.options.selectorResult)

        this.DOM.$$answerInputs.forEach($el => $el.addEventListener('click', this.onAnswerInputClick.bind(this), false))
        this.DOM.$el.addEventListener('submit', this.onSubmit.bind(this), false)
    }

    onAnswerInputClick(e) {
        const step = parseInt(e.currentTarget.dataset.identifierStep, 10)

        this.DOM.$$answerInputs.forEach($el => {
            if (parseInt($el.dataset.identifierStep) > step) {
                $el.checked = false
            }
        })

        this.update(false)
    }

    onSubmit(e) {
        e.preventDefault()
        this.update(true)
    }

    update(report) {
        const data = new FormData(this.DOM.$el)
        const url = this.DOM.$el.action

        if (report === true) {
            data.append('report', 1)
        } else {
            this.DOM.$result.innerHTML = ''
        }

        document.body.classList.toggle(this.options.classNameIsLoading, true)

        axios.post(url, data).then(response => {
            document.body.classList.toggle(this.options.classNameIsLoading, false)

            if (response.data.location !== undefined) {
                window.location.href = response.data.location
            } else {
                if (response.data.step !== undefined) {
                    const $step = this.DOM.$el.querySelector(`${this.options.selectorStep}[data-identifier-step='${response.data.step}']`)
                    this.DOM.$el.dataset.identifierCurrentStep = response.data.step

                    if ($step) {
                        $step.scrollIntoView({
                            behavior: 'smooth',
                            block: 'start'
                        })
                    }
                }

                if (response.data.result_html !== undefined) {
                    this.DOM.$result.innerHTML = response.data.result_html
                    this.DOM.$result.scrollIntoView({
                        behavior: 'smooth',
                        block: 'start'
                    })
                }
            }
        })
    }
}

Identifier.prototype.defaults = {
    classNameIsLoading: 'is-loading',
    selectorAnswerInput: '.js-identifierAnswerInput',
    selectorResult: '.js-identifierResult',
    selectorStep: '.js-identifierStep',
}