export default class Map
{
    constructor($el, options = {}) {
        this.DOM = { $el }
        this.options = { ...Map.prototype.defaults, ...options }
        this.init()
    }

    init() {
        mapboxgl.accessToken = 'pk.eyJ1IjoidWZvbWVsZHB1bnQiLCJhIjoiY2tpa3JmbWxrMGNsdjJxbDQ0OHh4OHR6cyJ9.6yFdFFGZV0zhOQrU5Jy52A'

        const options = {
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            zoom: 8,
            center: [5.291266, 52.132633]
        }

        this.map = new mapboxgl.Map(options)
        this.map.addControl(new mapboxgl.NavigationControl(), 'top-left')
    }
}

Map.prototype.defaults = {
}