export default class LocationPicker
{
    constructor($el, options = {}) {
        this.DOM = { $el }
        this.options = { ...LocationPicker.prototype.defaults, ...options }
        this.init()
    }

    init() {
        this.DOM.$$inputs = this.DOM.$el.querySelectorAll(this.options.selectorInput)
        this.DOM.$inputArea = this.DOM.$el.querySelector(`input[name='${this.options.inputNameArea}']`)
        this.DOM.$inputCity = this.DOM.$el.querySelector(`input[name='${this.options.inputNameCity}']`)
        this.DOM.$inputCountry = this.DOM.$el.querySelector(`input[name='${this.options.inputNameCountry}']`)
        this.DOM.$objectLat = this.DOM.$el.querySelector(`input[name='${this.options.inputNameObjectLat}']`)
        this.DOM.$objectLng = this.DOM.$el.querySelector(`input[name='${this.options.inputNameObjectLng}']`)
        this.DOM.$observerLat = this.DOM.$el.querySelector(`input[name='${this.options.inputNameObserverLat}']`)
        this.DOM.$observerLng = this.DOM.$el.querySelector(`input[name='${this.options.inputNameObserverLng}']`)

        this.markers = {
            object: {
                lat: null,
                lng: null,
                marker: null,
            },
            observer: {
                lat: null,
                lng: null,
                marker: null,
            },
        }
        this.mode = null
        
        this.DOM.$$inputs.forEach($input => $input.addEventListener('click', this.onInputClick.bind(this), false))

        this.initMap()
        this.setMode('observer')
    }

    initMap() {
        var options = {
            container: 'map',
            // style: 'https://api.maptiler.com/maps/basic/style.json?key=JQce5VgdpWitjFWrWEiq',
            style: 'https://basemaps.cartocdn.com/gl/positron-gl-style/style.json',
            zoom: 10,
            center: [ 5.291266, 52.132633 ]
        }

        this.map = new mapboxgl.Map(options)
        this.map.addControl(new mapboxgl.NavigationControl(), 'bottom-left')
        this.map.fitBounds([[ 3.25, 50.7 ], [ 7.35, 53.5 ]])
        this.map.on('click', this.onMapClick.bind(this))
    }

    onInputClick(e) {
        this.setMode(e.currentTarget.value)
    }

    onMapClick(e) {
        const { lng, lat } = e.lngLat

        if (this.markers[this.mode].marker) {
            this.markers[this.mode].marker.remove()
        }

        let $marker = document.createElement('div')
        $marker.className = `marker-${this.mode}`
        $marker.dataset.locationType = this.mode

        const options = {
            offset: new mapboxgl.Point(0, 4), 
            anchor: 'bottom',
            draggable: true,
            element: $marker,
        }

        const marker = new mapboxgl.Marker(options)
            .setLngLat(e.lngLat)
            .addTo(this.map)

        marker.on('dragend', (e) => {
            const { lng, lat } = e.target.getLngLat()
            this.setLocation(e.target, lat, lng)
        })

        marker.on('dragstart', (e) => {
            this.setMode(e.target.getElement().dataset.locationType)
        })

        this.setLocation(marker, lat, lng)

        const $tab = this.DOM.$el.querySelector(`${this.options.selectorTab}[data-location-picker-tab='${this.mode}']`)
        $tab.classList.add(this.options.classNameIsDone)

        if (this.mode === 'observer' && !this.markers.object.marker) {
            this.setMode('object')
        }
    }

    setLocation(marker, lat, lng) {
        const mode = marker.getElement().dataset.locationType
        this.markers[mode] = { lat, lng, marker }

        if (mode === 'observer') {
            const key = '65a911104e494985b0389a17cc472432'
            const url = `https://api.bigdatacloud.net/data/reverse-geocode-with-timezone?latitude=${lat}&longitude=${lng}&localityLanguage=nl&key=${key}`
    
            axios.get(url).then((response) => {
                const area = response.data.principalSubdivision
                const city = response.data.localityInfo.administrative.pop().name
                const country = response.data.countryCode

                this.DOM.$inputArea.value = area
                this.DOM.$inputCity.value = city
                this.DOM.$inputCountry.value = country
    
                console.log(`${city}, ${area}, ${country}`)
            })

            this.DOM.$observerLat.value = lat
            this.DOM.$observerLng.value = lng
        }
        
        if (mode === 'object') {
            this.DOM.$objectLat.value = lat
            this.DOM.$objectLng.value = lng  
        }
    }

    setMode(mode) {
        if (mode !== this.mode) {
            this.DOM.$$inputs.forEach($input => {
                if ($input.value === mode) {
                    $input.checked = true
                }
            })

            this.mode = mode
        }
    }
}

LocationPicker.prototype.defaults = {
    classNameIsDone: 'is-done',
    inputNameArea: 'location_area',
    inputNameCity: 'location_city',
    inputNameCountry: 'location_country',
    inputNameObjectLat: 'object_lat',
    inputNameObjectLng: 'object_lng',
    inputNameObserverLat: 'observer_lat',
    inputNameObserverLng: 'observer_lng',
    selectorInput: '.js-locationPickerInput',
    selectorTab: '.js-locationPickerTab',
}