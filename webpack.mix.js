const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/assets/js')
    .sass('resources/css/app.scss', 'public/assets/css')
    .sourceMaps()
    .browserSync({
        files: [
            'public/assets/css/*.css',
            'public/assets/js/*.js',
        ],
        proxy: 'localhost'
    });